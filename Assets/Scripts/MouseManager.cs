﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.Events;

[Serializable]
public class EventMove : UnityEvent<Vector3> { }

public class MouseManager : MonoBehaviour
{
    [SerializeField] private LayerMask clickableLayer;

    public EventMove OnClickEnviroment;

    private Camera mainCamera;
    private int leftClick = 0;
    private int maxClickDistance = 30;

    void Start()
    {
        mainCamera = Camera.main;
    }
    
    void Update()
    {
        RaycastHit hit;
        if (Physics.Raycast(mainCamera.ScreenPointToRay(Input.mousePosition), out hit, maxClickDistance, clickableLayer.value))
        {
            if (Input.GetMouseButtonDown(leftClick))            
                OnClickEnviroment?.Invoke(hit.point);          
        }
    }
}
