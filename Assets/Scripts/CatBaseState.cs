﻿
public abstract class CatBaseState
{
    public virtual void EnterState(CatControllerFsm catCtrlFsm)
    {
        UiManager.Instance.updateReactionDelegate?.Invoke(catCtrlFsm.CurrentReaction);

        UiManager.Instance.updateMoodDelegate?.Invoke(catCtrlFsm.Mood);

        catCtrlFsm.playButton.onClick.AddListener(() =>
            catCtrlFsm.TransitionToState(catCtrlFsm.playState, catCtrlFsm.CurrentReaction, catCtrlFsm.Mood));

        catCtrlFsm.feedButton.onClick.AddListener(() =>
            catCtrlFsm.TransitionToState(catCtrlFsm.feedState, catCtrlFsm.CurrentReaction, catCtrlFsm.Mood));

        catCtrlFsm.patButton.onClick.AddListener(() =>
            catCtrlFsm.TransitionToState(catCtrlFsm.patState, catCtrlFsm.CurrentReaction, catCtrlFsm.Mood));

        catCtrlFsm.kickButton.onClick.AddListener(() =>
           catCtrlFsm.TransitionToState(catCtrlFsm.kickState, catCtrlFsm.CurrentReaction, catCtrlFsm.Mood));
    }

    public abstract void UpdateState(CatControllerFsm catCtrlFsm);
}
