﻿using System.Collections;
using UnityEngine;

public class CatPlayState : CatBaseState, IReactionInAction
{
    private int pointIndexToCome;   

    public override void EnterState(CatControllerFsm catCtrlFsm)
    {
        StartReactionInAction(catCtrlFsm);

        base.EnterState(catCtrlFsm);        
    }
   
    public override void UpdateState(CatControllerFsm catCtrlFsm)
    {
        if (catCtrlFsm.CurrentMood == Mood.Good)
        {            
            catCtrlFsm.GetComponent<CatController>().Animator.SetFloat("Speed", 
                catCtrlFsm.GetComponent<CatController>().Agent.velocity.sqrMagnitude);

            catCtrlFsm.GetComponent<CatController>().Agent.destination = catCtrlFsm.
                GetComponent<CatController>().Ball.transform.position;
           
        }
        
        else if (catCtrlFsm.CurrentMood == Mood.Great)
        {
            catCtrlFsm.GetComponent<CatController>().Animator.SetFloat("Speed",
                catCtrlFsm.GetComponent<CatController>().Agent.velocity.sqrMagnitude);
            
            catCtrlFsm.StartCoroutine(RunLikeCrazy(catCtrlFsm));
        }
    }

    public void StartReactionInAction(CatControllerFsm catCtrlFsm)
    {
        var catCtrl = catCtrlFsm.GetComponent<CatController>();
        catCtrlFsm.CurrentMood = catCtrlFsm.Mood;

        switch (catCtrlFsm.Mood)
        {           
            case Mood.Bad:                
                catCtrlFsm.CurrentReaction = Reaction.Sits;
                break;

            case Mood.Good:               
                catCtrlFsm.CurrentReaction = Reaction.SlowlyRunsForBall;
                catCtrl.Animator.SetBool("Eat", false);
                catCtrl.Agent.speed = catCtrl.NormalSpeed;
                catCtrlFsm.Mood = Mood.Great;
                break;

            case Mood.Great:
                catCtrlFsm.StopAllCoroutines();

                catCtrl.Animator.SetBool("Purr", false);
                catCtrl.Animator.SetBool("Eat", false);
                catCtrl.Agent.speed = 6f;

                catCtrlFsm.CurrentReaction = Reaction.RunsLikeCrazy;
                pointIndexToCome = Random.Range(0, catCtrl.waypoints.Length);
                break;
        }
    }

    private IEnumerator RunLikeCrazy(CatControllerFsm catCtrlFsm)
    {                    
        catCtrlFsm.GetComponent<CatController>().Agent.destination = catCtrlFsm.GetComponent<CatController>().
            waypoints[pointIndexToCome].position;
      
        yield return new WaitUntil(() => Vector3.SqrMagnitude(catCtrlFsm.GetComponent<CatController>().waypoints[pointIndexToCome].position
            - catCtrlFsm.transform.position) <= catCtrlFsm.GetComponent<CatController>().Agent.stoppingDistance);
        
        pointIndexToCome = Random.Range(0, catCtrlFsm.GetComponent<CatController>().waypoints.Length);       
    }       
}
