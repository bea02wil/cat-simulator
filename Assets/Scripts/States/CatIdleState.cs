﻿
public class CatIdleState : CatBaseState
{
    public override void EnterState(CatControllerFsm catCtrlFsm)
    {             
        //TODO: После записи видео о тестировании проекта, сгенирировать рандмоное настроение на старте
        var catController = catCtrlFsm.GetComponent<CatController>();
        catController.Animator.Play("Base Layer.Idle");

        base.EnterState(catCtrlFsm);      
    }

    public override void UpdateState(CatControllerFsm catCtrlFsm)
    {
        
    }
}
