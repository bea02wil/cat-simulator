﻿
public class CatPatState : CatBaseState, IReactionInAction
{
    public override void EnterState(CatControllerFsm catCtrlFsm)
    {
        StartReactionInAction(catCtrlFsm);

        base.EnterState(catCtrlFsm);     
    }
  
    public override void UpdateState(CatControllerFsm catCtrlFsm)
    {
        
    }

    public void StartReactionInAction(CatControllerFsm catCtrlFsm)
    {
        var catCtrl = catCtrlFsm.GetComponent<CatController>();      

        catCtrl.Animator.SetFloat("Speed", 0);

        switch (catCtrlFsm.Mood)
        {
            case Mood.Bad:
                catCtrlFsm.CurrentReaction = Reaction.Scratches;
                break;

            case Mood.Good:
                catCtrlFsm.CurrentReaction = Reaction.Purrs;
                catCtrlFsm.Mood = Mood.Great;

                catCtrl.Agent.speed = 0;
                catCtrl.Animator.SetBool("Eat", false);
                catCtrl.Animator.SetBool("Purr", true);
                break;

            case Mood.Great:
                catCtrlFsm.CurrentReaction = Reaction.PurrsAndMovesTail;
                catCtrl.Agent.speed = 0;             
                catCtrl.Animator.SetBool("Purr", true);
                break;
        }
    }
}
