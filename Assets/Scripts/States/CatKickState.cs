﻿
public class CatKickState : CatBaseState, IReactionInAction
{
    public override void EnterState(CatControllerFsm catCtrlFsm)
    {
        StartReactionInAction(catCtrlFsm);

        base.EnterState(catCtrlFsm);      
    }
   
    public override void UpdateState(CatControllerFsm catCtrlFsm)
    {
        if (catCtrlFsm.CurrentMood == Mood.Good)
        {
            catCtrlFsm.GetComponent<CatController>().Animator.
                SetFloat("Speed", catCtrlFsm.GetComponent<CatController>().Agent.velocity.sqrMagnitude);
          
        }
    }

    public void StartReactionInAction(CatControllerFsm catCtrlFsm)
    {
        var catCtrl = catCtrlFsm.GetComponent<CatController>();
        catCtrlFsm.CurrentMood = catCtrlFsm.Mood;
        

        switch (catCtrlFsm.Mood)
        {
            case Mood.Bad:
                catCtrlFsm.CurrentReaction = Reaction.JumpsAndBitesEar;               
                break;

            case Mood.Good:
                catCtrl.Animator.SetBool("Eat", false);
                catCtrlFsm.StopAllCoroutines();
                catCtrl.Agent.destination = catCtrl.fakeCarpetPivot.position;
                catCtrl.Agent.speed = catCtrl.NormalSpeed;

                catCtrlFsm.CurrentReaction = Reaction.RunsToCarpetAndPisses;
                catCtrlFsm.Mood = Mood.Bad;
                break;

            case Mood.Great:
                catCtrlFsm.CurrentReaction = Reaction.RunsToAnotherRoom;
                catCtrlFsm.Mood = Mood.Bad;

                catCtrl.Agent.speed = 0;
                catCtrl.Animator.SetBool("Purr", false);
                catCtrl.Animator.SetFloat("Speed", 0);                              
                break;
        }
    }
}
