﻿using System.Collections;
using UnityEngine;

public class CatFeedState : CatBaseState, IReactionInAction
{   
    public override void EnterState(CatControllerFsm catCtrlFsm)
    {
        StartReactionInAction(catCtrlFsm);

        base.EnterState(catCtrlFsm);      
    }
  
    public override void UpdateState(CatControllerFsm catCtrlFsm)
    {
        catCtrlFsm.GetComponent<CatController>().Animator.
            SetFloat("Speed", catCtrlFsm.GetComponent<CatController>().Agent.velocity.sqrMagnitude);

        if (Vector3.SqrMagnitude(catCtrlFsm.GetComponent<CatController>().bowl.position
            - catCtrlFsm.transform.position) <= catCtrlFsm.GetComponent<CatController>().Agent.stoppingDistance)
        {
            if (catCtrlFsm.CurrentMood == Mood.Bad)
            {               
                catCtrlFsm.StartCoroutine(PlayEatAnimation(catCtrlFsm, 8.3f));

                if (catCtrlFsm.GetComponent<CatController>().Animator.GetCurrentAnimatorStateInfo(0).IsName("Eat"))
                {
                    if (Vector3.SqrMagnitude(catCtrlFsm.transform.position
                        - catCtrlFsm.GetComponent<CatController>().Player.transform.position) <= 3)
                    {
                        UiManager.Instance.updateReactionDelegate?.Invoke(Reaction.Scratches);
                        //Here should be transition to cat scratches state but i dont have scratch animation
                    }

                    else { UiManager.Instance.updateReactionDelegate?.Invoke(catCtrlFsm.CurrentReaction); }
                }              
            }

            else           
                catCtrlFsm.StartCoroutine(PlayEatAnimation(catCtrlFsm, 2.6f));                                                         
        }
    }

    public void StartReactionInAction(CatControllerFsm catCtrlFsm)
    {
        var catCtrl = catCtrlFsm.GetComponent<CatController>();

        catCtrl.Agent.speed = catCtrl.NormalSpeed;
        catCtrl.Agent.destination = catCtrl.bowl.position;      
        catCtrlFsm.CurrentMood = catCtrlFsm.Mood;
        catCtrlFsm.StopAllCoroutines();

        switch (catCtrlFsm.Mood)
        {           
            case Mood.Bad:               
                catCtrlFsm.CurrentReaction = Reaction.EatsEverythingCanScratch;              
                catCtrlFsm.Mood = Mood.Good;                             
                break;

            case Mood.Good:                
                catCtrlFsm.CurrentReaction = Reaction.EatsEverythingQuickly;                
                catCtrlFsm.Mood = Mood.Great;              
                break;

            case Mood.Great:                
                catCtrl.Animator.SetBool("Purr", false);               
                catCtrl.Animator.SetFloat("Speed", 0);

                catCtrlFsm.CurrentReaction = Reaction.EatsEverythingQuickly;              
                break;
        }
    }

    //slow or fast eating
    private IEnumerator PlayEatAnimation(CatControllerFsm catCtrlFsm, float timeToWait)
    {
        catCtrlFsm.GetComponent<CatController>().Animator.SetBool("Eat", true);

        yield return new WaitForSeconds(timeToWait);

        catCtrlFsm.GetComponent<CatController>().Animator.SetBool("Eat", false);       

        if (catCtrlFsm.GetComponent<CatController>().Animator.GetCurrentAnimatorStateInfo(0).IsName("Eat"))
        {           
            catCtrlFsm.TransitionToState(catCtrlFsm.idleState,  mood: catCtrlFsm.Mood);
        }
    }
}
