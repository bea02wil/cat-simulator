﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class UiManager : Manager<UiManager>
{
    [SerializeField] private Text currentReaction;
    [SerializeField] private Image moodIndicator;
    [SerializeField] private Slider currentMood;

    public Action<string> updateReactionDelegate;
    public Action<Mood> updateMoodDelegate;

    protected override void Awake()
    {  
        base.Awake();
             
        updateReactionDelegate = UpdateReaction;
        updateMoodDelegate = UpdateMood;
    }

    public void UpdateReaction(string reaction)
    {
        currentReaction.text = reaction;
    }

    public void UpdateMood(Mood mood)
    {
        var sliderStartPosition = 0f;
        var sliderMiddlePosition = 0.5f;
        var sliderEndPosition = 1f;

        switch (mood)
        {
            case Mood.Bad:
                currentMood.value = sliderStartPosition;
                moodIndicator.color = Color.red;
                break;

            case Mood.Good:
                currentMood.value = sliderMiddlePosition;
                moodIndicator.color = Color.yellow;
                break;

            case Mood.Great:
                currentMood.value = sliderEndPosition;
                moodIndicator.color = Color.green;
                break;
        }
    } 
}
