﻿
public static class Reaction
{
    public const string Sits = "Сидит на месте";

    public const string SlowlyRunsForBall = "Медленно бегает за мячиком";

    public const string RunsLikeCrazy = "Носится, как угорелая";

    public const string EatsEverythingCanScratch = "Все съедает, но если в это время подойти - поцарапает";

    public const string EatsEverythingQuickly = "Быстро все съедает";

    public const string Scratches = "Царапает";

    public const string Purrs = "Мурлычет";

    public const string PurrsAndMovesTail = "Мурлычет и виляет хвостом";

    public const string JumpsAndBitesEar = "Прыгает и кусает за правое ухо";

    public const string RunsToAnotherRoom = "Убегает в другую комнату";

    public const string RunsToCarpetAndPisses = "Убегает на ковер и писает";

    public const string Inactivity = "Без действия";
}
