﻿using UnityEngine.SceneManagement;

public class GameManager : Manager<GameManager>
{
    public void LoadLevel(string levelName)
    {
        SceneManager.LoadScene(levelName);
    }
}
