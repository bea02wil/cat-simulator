﻿using UnityEngine;
using UnityEngine.UI;

public enum Mood { Bad, Good, Great }

public class CatControllerFsm : MonoBehaviour
{        
    public Button playButton;
    public Button feedButton;
    public Button patButton;
    public Button kickButton;

    private CatBaseState currentState;
    private string currentReaction;
    private Mood mood;       

    public readonly CatIdleState idleState = new CatIdleState();
    public readonly CatPlayState playState = new CatPlayState();
    public readonly CatFeedState feedState = new CatFeedState();
    public readonly CatPatState patState = new CatPatState();
    public readonly CatKickState kickState = new CatKickState();

    public Mood CurrentMood { get; set; }
    public string CurrentReaction { get => currentReaction; set { currentReaction = value; } }
    public Mood Mood { get => mood; set { mood = value; } }


    private void Awake()
    {
        currentReaction = Reaction.Inactivity;
    }

    private void Start()
    {
        Mood startMood = (Mood)Random.Range(0, 3);

        TransitionToState(idleState, currentReaction, startMood);     
    }

    private void Update()
    {
        currentState.UpdateState(this);
    }

    public void TransitionToState(CatBaseState state, string reaction = Reaction.Inactivity, Mood mood = Mood.Bad)
    {
        RemoveButtonsListeners();

        currentState = state;
        this.mood = mood;
        currentReaction = reaction;      
        currentState.EnterState(this);       
    }

    private void RemoveButtonsListeners()
    {
        var buttons = new Button[] { playButton, feedButton, patButton, kickButton };

        foreach (var button in buttons)
        {
            button.onClick?.RemoveAllListeners();
        }
    }
}
