﻿using UnityEngine;

public class CarpetController : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {      
        if (other.tag == "Cat")
        {
            var catCtrl = other.gameObject.GetComponent<CatController>();
            var catCtrlFsm = catCtrl.GetComponent<CatControllerFsm>();

            if (catCtrlFsm.CurrentReaction == Reaction.RunsToCarpetAndPisses)
                catCtrl.pissEffect.Play();
        }
    }
}
