﻿using UnityEngine;
using UnityEngine.AI;

public class CatController : MonoBehaviour
{
    [SerializeField] private AudioClip purrSound;

    public Transform bowl;
    public Transform fakeCarpetPivot;
    public Transform[] waypoints;
    public ParticleSystem pissEffect;

    private PlayerController player;
    private Animator animator;
    private NavMeshAgent agent;
    private BallController ball;   
    private AudioSource audioSource;

    private float normalSpeed;
    private float purrSoundVolume = 0.6f;

    public Animator Animator { get => animator; }
    public NavMeshAgent Agent { get => agent; }
    public BallController Ball { get => ball; }    
    public PlayerController Player { get => player; }
    public float NormalSpeed { get => normalSpeed; }

    private void Awake()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
        animator = GetComponent<Animator>();
        agent = GetComponent<NavMeshAgent>();
        ball = FindObjectOfType<BallController>();
        audioSource = GetComponent<AudioSource>();
        normalSpeed = agent.speed;
    }

    public void Purr() // for Event purrs animation
    {
        audioSource.PlayOneShot(purrSound, purrSoundVolume);
    }  
}
