﻿using UnityEngine;

public class BallController : MonoBehaviour
{
    [SerializeField] private float forceToAdd;
    
    private Rigidbody rigidBody;
    
    private void Awake()
    {
        rigidBody = GetComponent<Rigidbody>();
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == "Cat")
        {
            var forceDirection = transform.position - other.transform.position;
            forceDirection.y += 1f;            
            forceDirection.Normalize();

            rigidBody.AddForce(forceDirection * forceToAdd);
        }
    }
}
